# Dankupdates
The dankbupdate project provides anachronous updates for enterprise linux deployments. This allows for staged delivery of updates. The dankbupdate server provides mirror support for linux updates while using snapshots.

![dankB](dankb.png "dankB")

* Easily create profiles and assign updates to those profiles at will.
* Removes risk of manufactures automatically breaking your production servers.
* Test updates before deploying them.
* Create historical updates to support backup restoration
* Reduced consumption of data resources through hard-links


## License
DankBUpdates is licensed under AGPL v3.0

