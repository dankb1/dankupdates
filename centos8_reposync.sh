#!/bin/bash
repos_base_dir="/data/repos/centos/8/"

# Start sync if base repo directory exist
if [[ -d "$repos_base_dir" ]] ; then
  # Start Sync
  rsync  -avSHP --delete rsync://mirror.liquidtelecom.com/centos/7/  "$repos_base_dir"
  # Download CentOS 8 repository key
  wget -P $repos_base_dir wget https://www.centos.org/keys/RPM-GPG-KEY-CentOS-Official
fi
